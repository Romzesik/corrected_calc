import java.util.Scanner;
import java.util.Arrays;

public class Main {
        static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws Throwable{
        int num1 = getInt();
        int num2 = getInt();
        char operation = getOperation();
        int result = calc(num1,num2,operation);
        System.out.println("Результат операции: "+result);
    }


    static {
        System.out.println("Введите имя: ");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.next();
        System.out.println("Привет " + name);
    }


    public static int getInt(){
        System.out.println("Введите число:");
        int num;
        if(scanner.hasNextInt()){
            num = scanner.nextInt();
        } else {
            System.out.println("Вы допустили ошибку при вводе числа. Попробуйте еще раз.");
            scanner.next();
            num = getInt();
        }
        return num;
    }





    private static char getOperation(){
        System.out.println("Выберите операцию(знаком):");//+,-,/,*
        System.out.println("\nСложение:+");
        System.out.println("Вычитание: -");
        System.out.println("Умножение: *");
        System.out.println("Деление: /");
        System.out.println("Ваш выбор: ");
        char operation;
        if(scanner.hasNext()){
            operation = scanner.next().charAt(0);
        } else {
            System.out.println("Вы допустили ошибку при вводе операции. Попробуйте еще раз.");
            scanner.next();
            operation = getOperation();
        }
        return operation;
    }

    private static int calc(int num1, int num2, char operation){

        int result;
        switch (operation){
            case '+':
                result = num1+num2;
                break;
            case '-':
                result = num1-num2;
                break;
            case '*':
                result = num1*num2;
                break;
            case '/':
                try {
                        result = num1 / num2;
                        break;
                } catch (ArithmeticException e) {
                    System.out.println("деление на ноль: " + e);
                }
            default:
                System.out.println("Операция не распознана. Повторите ввод.");
                result = calc(num1, num2, getOperation());
        }
        return result;
    }
}